from django.urls import path
from . import views

urlpatterns = [
    path('create', views.Create.as_view(), name='create'),
    path('<slug:redir_path>', views.Redirect.as_view(), name='redir'),
    path('', views.Index.as_view(), name='index'),
]
